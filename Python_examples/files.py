#! /usr/bin/env python

#
# you can download the files regarding airports from:
# https://raw.githubusercontent.com/jpatokal/openflights/master/data/airports.dat
# and
# https://raw.githubusercontent.com/jpatokal/openflights/master/data/routes.dat
#

# change this path to your specific situation
file_routes = open("../houdini/routes.dat.txt", 'r')

counter = 0
while counter < 10:
    current_line = file_routes.readline()
    print current_line
    counter += 1

file_routes.close()

class Airport(object):
    def __init__(self, airport_name, country, airport_code):
        self.airport_name = airport_name
        self.country      = country
        self.airport_code = airport_code

airport_list = []
with open("../houdini/airports.dat.txt", 'r') as file_routes:
    for current_line in file_routes:
        words = current_line.split(",")
        new_airport = Airport(words[1], words[3], words[4])
        airport_list.append(new_airport)

for i in range(10):
    current_airport = airport_list[i]
    print ("Airport: %s, Country: %s, Code: %s" % 
           (current_airport.airport_name,
            current_airport.country,
            current_airport.airport_code))

print "Number of objects: %d" % len(airport_list)
