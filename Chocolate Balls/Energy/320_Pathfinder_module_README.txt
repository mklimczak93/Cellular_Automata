 _____        _    _       __  _             _               __  __             _         _       
 |  __ \      | |  | |     / _|(_)           | |             |  \/  |           | |       | |      
 | |__) |__ _ | |_ | |__  | |_  _  _ __    __| |  ___  _ __  | \  / |  ___    __| | _   _ | |  ___ 
 |  ___// _` || __|| '_ \ |  _|| || '_ \  / _` | / _ \| '__| | |\/| | / _ \  / _` || | | || | / _ \
 | |   | (_| || |_ | | | || |  | || | | || (_| ||  __/| |    | |  | || (_) || (_| || |_| || ||  __/
 |_|    \__,_| \__||_| |_||_|  |_||_| |_| \__,_| \___||_|    |_|  |_| \___/  \__,_| \__,_||_| \___|
                                                                                                   
Author: Alex Kypriotakis-Weijers
Planet Makers Spring 2018
Energy Group


This hudini file contains some set of SOP trees with find shortest path SOPs. Its purpose is to simulate distribution paths from 
one point or points of the globe to other points. In every tree you can sellect your starting position
points in the Start Group SOP and your end points at the End Group SOP. If you wish to see the max reach of your paths with no End 
Points assigned then you can leave it blank. The subdivide SOP defines the paths between the triangles.
If you press within the groups to select start/end points you will see the planet in a "hexagonal" mode instead of triangular.

Within the Find Shortest Path SOP you can select which constraints and avoidances you want your pathfinder to follow. In the file you
 can find trees for ship, train and truck transport. Bellow you can see the default constraints which are all based on actual globe
data.

        Movement max:      Can Cross Over:

Ship:        10               Sea Tiles  

Train:        6               Land Tiles 

Truck:        4               Land & Mount Tiles


After you have set up your pathfinder you can make an Attribute Wrangle SOP to set a certain value change from the source towards the
target points. You can also incorporate the "cost" attribute from the Find Shortest Path SOP to add loss of an attribute as it travels
further from the starting points.
 

Enjoy and I hope it proves useful to ya! =)